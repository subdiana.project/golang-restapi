package router

import (
	"log"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
)

// Root --> Root For Router
func Root() *gin.Engine {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error Load .env File")
	}

	route := gin.Default()

	// File On V1.go
	Version1(route)

	return route
}
