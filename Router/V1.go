package router

import (
	Ctrl "../Controller"
	"github.com/gin-gonic/gin"
)

// Version1 --> api/v1/..
func Version1(route *gin.Engine) {

	route.GET("/", Ctrl.Index)
	// api/v1/{group}
	V1 := route.Group("/api/v1.0")

	// Use Middleware

	// Router Child
	V1.GET("/user", Ctrl.Users)

	// Router Child Carfix
	V1.GET("/carfix", Ctrl.CarfixToAfi)
	V1.GET("/carfix/:id", Ctrl.CarfixFind)

	// Router Child bayauct
	V1.GET("/bayauct", Ctrl.GetDataBayauct)
	V1.GET("/bayauct/:merk", Ctrl.GetMerkBayauct)
	V1.GET("/bayauct/:merk/:id", Ctrl.GetIDBayauct)

	V1.POST("/bayauct/post", Ctrl.PostBayauctIn)

}
