package model

import (
	Struct "../Struct"
)

// GetBayauct -- get data tb_bayauct
func GetBayauct() []Struct.Bayauct {
	var bayauct []Struct.Bayauct
	db.Find(&bayauct)
	return bayauct
}

// GetBayauctMerk -- Get data by merk
func GetBayauctMerk(merk string) []Struct.Bayauct {
	var bayauct []Struct.Bayauct
	db.Where("merk = ?", merk).Find(&bayauct)
	return bayauct
}

// GetBayauctID -- get data by id
func GetBayauctID(id string) Struct.Bayauct {
	var bayauct Struct.Bayauct
	db.Where("id = ?", id).Find(&bayauct)
	return bayauct
}
