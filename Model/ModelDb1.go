package model

import (
	Struct "../Struct"
)

// GetUsers Get Data User
func GetUsers() []Struct.User {
	var Users []Struct.User
	db.Find(&Users)
	return Users
}

// GetCarfixAfi -- Model Carfix to AFI
func GetCarfixAfi() []Struct.CarfixToAfi {
	var carfix []Struct.CarfixToAfi
	db.Find(&carfix)
	return carfix
}

// GetCarfixOne -- Get By VehicleCode
func GetCarfixOne(id string) Struct.CarfixToAfi {
	var carfix Struct.CarfixToAfi
	db.Where("vehicle_code = ?", id).Find(&carfix)
	return carfix
}
