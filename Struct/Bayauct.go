package TypeStruct

type (

	// Output

	// BayauctOut -- Output Response
	BayauctOut struct {
		ID         uint       `json:"id"`
		Mobil      Mobils     `json:"Mobil"`
		Harga      Harga      `json:"Harga"`
		Keterangan Keterangan `json:"Keterangan"`
	}

	// Mobils -- Format response mobil
	Mobils struct {
		NoPolisi string `json:"NoPolisi"`
		Merk     string `json:"Merk"`
		Kategori string `json:"Kategori"`
		Model    string `json:"Model"`
		Tahun    string `json:"Tahun"`
		Warna    string `json:"Warna"`
	}
	// Harga -- format response harga
	Harga struct {
		HargaLimit  int `json:"HargaLimit"`
		HargaSukses int `json:"HargaSukses"`
	}
	// Keterangan -- Format Response Keterangan
	Keterangan struct {
		Result    string `json:"Result"`
		TglUpload string `json:"TglUpload"`
		Area      string `json:"Area"`
	}

	// Bayauct -- Model From Database
	Bayauct struct {
		ID          uint
		Merk        string
		Kategori    string
		Model       string
		Tahun       string
		Warna       string
		NoPolisi    string
		HargaLimit  int
		HargaSukses int
		Result      string
		TglUpload   string
		Area        string
	}

	// BayauctIn -- input by bindJSON
	BayauctIn struct {
		Merk     string `validate:"required"`
		Kategori string `validate:"required"`
		Model    string `validate:"required"`
		Tahun    string `validate:"required"`
		Warna    string `validate:"required"`
		NoPolisi string `validate:"required"`
	}
)

// TableName -- Custom name Table
func (Bayauct) TableName() string {
	return "tb_bayauct"
}
