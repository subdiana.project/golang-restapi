package TypeStruct

type (

	// Output

	// Carfixs -- Output Json
	Carfixs struct {
		No          int    `json:"Index"`
		VehicleCode string `json:"VehicleCode"`
		Mobil       Mobil  `json:"Mobil"`
	}

	// Mobil -- Child Carfixs Struct
	Mobil struct {
		Deskripsi  string `json:"Deskripsi"`
		TahunMobil string `json:"TahunKendaraan"`
	}

	// CarfixToAfi -- Struct From Database
	CarfixToAfi struct {
		VehicleCode string `json:"VehicleCode"`
		Deskripsi   string `json:"Deskripsi"`
		TahunMobil  string `json:"TahunKendaraan"`
	}
)

// TableName -- get Custom Name Table
func (CarfixToAfi) TableName() string {
	return "carfix_to_afi"
}
