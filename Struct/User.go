package TypeStruct

type (
	// Users -- Output Users
	Users struct {
		ID   uint `json:"Id"`
		Auth Aut  `json:"Auth"`
	}
	// Aut -- Output Aut
	Aut struct {
		Username string `json:"Username"`
		Password string `json:"Password"`
		Bio      Bio    `json:"BIO"`
	}
	// Bio -- Output Bio
	Bio struct {
		Nama   string `json:"Nama"`
		Alamat string `json:"Alamat"`
		Ttgl   string `json:"Ttgl"`
	}

	// User -- Get Data From Database
	User struct {
		ID       uint
		Username string
		Password string
	}
)

// TableName Change Custom Name Table
func (User) TableName() string {
	return "tb_users"
}
