package handle

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// NotFound -- 404 handle
func NotFound(res *gin.Context) {
	res.JSON(http.StatusNotFound, gin.H{
		"code":    http.StatusNotFound,
		"message": "Data Not Found",
		"result":  map[string]interface{}{},
	})
}
