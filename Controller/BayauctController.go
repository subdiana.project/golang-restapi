package controller

import (
	"net/http"

	handle "../Middleware/Handle"
	Model "../Model"
	structs "../Struct"
	"github.com/gin-gonic/gin"
	validator "gopkg.in/go-playground/validator.v9"
)

// GetDataBayauct -- controller Get data bayauct
func GetDataBayauct(res *gin.Context) {
	data := Model.GetBayauct()
	if len(data) == 0 {
		handle.NotFound(res)
		return
	}

	var output []structs.BayauctOut
	for _, i := range data {
		output = append(output,
			structs.BayauctOut{
				ID: i.ID,
				Mobil: structs.Mobils{
					NoPolisi: i.NoPolisi,
					Merk:     i.Merk,
					Kategori: i.Kategori,
					Model:    i.Model,
					Tahun:    i.Tahun,
					Warna:    i.Warna,
				},
				Harga: structs.Harga{
					HargaLimit:  i.HargaLimit,
					HargaSukses: i.HargaSukses,
				},
				Keterangan: structs.Keterangan{
					Result:    i.Result,
					TglUpload: i.TglUpload,
					Area:      i.Area,
				},
			})
	}

	res.JSON(http.StatusOK, gin.H{
		"code":    http.StatusOK,
		"message": "OK",
		"result":  output,
	})
}

// GetMerkBayauct -- get data bayaut by Merk
func GetMerkBayauct(res *gin.Context) {
	merk := res.Param("merk")
	data := Model.GetBayauctMerk(merk)
	if len(data) == 0 {
		handle.NotFound(res)
		return
	}

	var output []structs.BayauctOut
	for _, i := range data {
		output = append(output,
			structs.BayauctOut{
				ID: i.ID,
				Mobil: structs.Mobils{
					NoPolisi: i.NoPolisi,
					Merk:     i.Merk,
					Kategori: i.Kategori,
					Model:    i.Model,
					Tahun:    i.Tahun,
					Warna:    i.Warna,
				},
				Harga: structs.Harga{
					HargaLimit:  i.HargaLimit,
					HargaSukses: i.HargaSukses,
				},
				Keterangan: structs.Keterangan{
					Result:    i.Result,
					TglUpload: i.TglUpload,
					Area:      i.Area,
				},
			})
	}

	res.JSON(http.StatusOK, gin.H{
		"code":    http.StatusOK,
		"message": "OK",
		"result":  output,
	})
}

// GetIDBayauct -- get data bayauct by id
func GetIDBayauct(res *gin.Context) {
	id := res.Param("id")
	data := Model.GetBayauctID(id)

	if data.Merk == "" {
		handle.NotFound(res)
		return
	}

	output := structs.BayauctOut{
		ID: data.ID,
		Mobil: structs.Mobils{
			NoPolisi: data.NoPolisi,
			Merk:     data.Merk,
			Kategori: data.Kategori,
			Model:    data.Model,
			Tahun:    data.Tahun,
			Warna:    data.Warna,
		},
		Harga: structs.Harga{
			HargaLimit:  data.HargaLimit,
			HargaSukses: data.HargaSukses,
		},
		Keterangan: structs.Keterangan{
			Result:    data.Result,
			TglUpload: data.TglUpload,
			Area:      data.Area,
		},
	}

	// out := append(structs.BayauctOut, data)

	res.JSON(http.StatusOK, gin.H{
		"code":    http.StatusOK,
		"message": "OK",
		"result":  output,
	})
}

// PostBayauctIn -- get data bindJSON POST Methods
func PostBayauctIn(res *gin.Context) {
	var post structs.BayauctIn

	if err := res.BindJSON(&post); err != nil {
		res.JSON(http.StatusForbidden, gin.H{
			"code":    http.StatusForbidden,
			"message": "Bad Request",
			"result":  map[string]string{},
		})
		return
	}

	// Validasi structs
	valid := validator.New()
	if er := valid.Struct(post); er != nil {
		res.JSON(http.StatusForbidden, gin.H{
			"code":    http.StatusForbidden,
			"message": er.Error(),
			"result":  map[string]string{},
		})
		return

	}

	res.JSON(http.StatusOK, gin.H{
		"code":    http.StatusOK,
		"message": "OK",
		"result":  post,
	})

}
