package controller

import (
	"net/http"

	handle "../Middleware/Handle"
	Model "../Model"
	structs "../Struct"
	"github.com/gin-gonic/gin"
)

// Index -- contoller For index
func Index(res *gin.Context) {
	res.JSON(http.StatusOK, gin.H{
		"code":    http.StatusOK,
		"message": "Ok",
		"result":  map[string]string{},
	})
}

// Users -- Get Data Users
func Users(res *gin.Context) {
	data := Model.GetUsers()
	if len(data) == 0 {
		handle.NotFound(res)
		return
	}

	var output []structs.Users
	for _, dt := range data {
		output = append(output, structs.Users{ID: dt.ID, Auth: structs.Aut{Username: dt.Username, Password: dt.Password, Bio: structs.Bio{Nama: dt.Username}}})
	}

	res.JSON(http.StatusOK, gin.H{
		"code":    http.StatusOK,
		"message": "OK",
		"result":  output,
	})
}

// CarfixToAfi -- Get Data Carfix Vs AfI
func CarfixToAfi(res *gin.Context) {
	data := Model.GetCarfixAfi()
	if len(data) == 0 {
		handle.NotFound(res)
		return
	}

	var output []structs.Carfixs
	for i, d := range data {
		output = append(output, structs.Carfixs{No: i + 1, VehicleCode: d.VehicleCode, Mobil: structs.Mobil{Deskripsi: d.Deskripsi, TahunMobil: d.TahunMobil}})
	}
	res.JSON(http.StatusOK, gin.H{
		"code":    http.StatusOK,
		"message": "OK",
		"result":  output,
	})
}

// CarfixFind -- find data by vehicle code
func CarfixFind(res *gin.Context) {
	id := res.Param("id")
	data := Model.GetCarfixOne(id)
	if data.VehicleCode == "" {
		handle.NotFound(res)
		return
	}

	res.JSON(http.StatusOK, gin.H{
		"code":    http.StatusOK,
		"message": "OK",
		"result":  data,
	})
}
