package main

import (
	"net/http"

	Router "./Router"
	"github.com/gin-gonic/gin"
)

func main() {
	serve := Router.Root()

	serve.NoRoute(func(res *gin.Context) {
		res.JSON(http.StatusNotFound, gin.H{
			"code":    http.StatusNotFound,
			"message": "Page Not Found",
			"result":  map[string]string{},
		})
	})

	serve.Run()
}
